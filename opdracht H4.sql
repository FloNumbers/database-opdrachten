/*OPDRACHT 4.1*/

/*Creates database*/
CREATE DATABASE Telefoon;

/*Creates different tables*/
CREATE TABLE Tbl_Telefoon(TelefoonID VARCHAR(80), MerkID INT, Type VARCHAR(80), Beschrijving VARCHAR(80), PRIMARY KEY (TelefoonID));
CREATE TABLE Tbl_Abonnement(AbonnementID VARCHAR(80), Telefoon VARCHAR(80), Beltijd DOUBLE, Prijs DOUBLE, PRIMARY KEY (AbonnementID, Telefoon));
CREATE TABLE Tbl_Merk(MerkID INT, Merk VARCHAR(80), Fabrikant VARCHAR(80), PRIMARY KEY (MerkID));


/*OPDRACHT 4.2*/

/*Drops database if exists*/
DROP DATABASE Factuur;

/*Creates database*/
CREATE DATABASE Factuur;

/*Creates different tables*/
CREATE TABLE Factuur(Factuurnr INT, Facdatum VARCHAR(80), Facomschrijving VARCHAR(80), PRIMARY KEY (Factuurnr));
CREATE TABLE Factuurregel(Facregel INT, Factuurnr INT, Prodnr INT, FacAantalProduct INT, PRIMARY KEY (Facregel, Factuurnr));
CREATE TABLE Product(Prodnr INT, Omschrijving VARCHAR(80), BTW INT, Prijs VARCHAR(80), PRIMARY KEY (Prodnr));
CREATE TABLE BTWcode(BTW INT, Percentage INT, PRIMARY KEY (BTW));